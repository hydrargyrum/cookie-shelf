# Cookie shelf

Cookie shelf is a Firefox web-extension that augments its [multi-account containers feature](https://addons.mozilla.org/en-GB/firefox/addon/multi-account-containers/) by allowing cookie control in those containers.

The base "Clear all cookies" feature is just too large. It doesn't take into account containers.
**You might want to keep cookies for site A, but only in container 1. If there are cookies for site A in container 2, you want to remove them.**

With Cookie shelf, you can:
- view all containers and the cookies that are in each container
- delete cookies from one site, but in one container only if desired
- delete all cookies of a container
- configure it for automatic enforcement of rules (allowlist/blocklist)

## Explore containers/cookies

In the settings panel of Cookie shelf, you will be presented a list of the containers and their cookies.

## Optional configuration

If you configure Cookie shelf with a few rules, it will automatically remove cookies not respecting the rules.

In the settings panel of Cookie shelf, there is a text-based rule editor. It should contain lines (rules) following a specific syntax.

Example configuration:

    firefox-container-2: -siteA

This syntax applies the following rule: *Everytime site A sets cookies in container 2, they are removed*.

    firefox-container-1: +siteA,-*

This syntax applies the following rule: *Everytime a site other than site A sets cookies in container 1, they are removed*.

## License

Cookie shelf is in the public domain.
