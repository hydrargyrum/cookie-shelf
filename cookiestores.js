window.addEventListener('load', () => {
    showCookies();
    document.getElementById('erase-button').addEventListener('click', eraseCookies);
});


function toList(elts) {
    let lst = [];
    for (let elt of elts) {
        lst.push(elt);
    }
    return lst;
}

function showCookies() {
    browser.contextualIdentities.query({}).then(identities =>
        browser.cookies.getAllCookieStores().then(stores => {
            let identitiesStore = [];
            for (let identity of identities) {
                identitiesStore[identity.cookieStoreId] = identity;
            }

            for (let store of stores) {
                if (store.id == 'firefox-default') {
                    identities.unshift({
                        cookieStoreId: store.id,
                        color: 'black',
                        colorCode: '#000000',
                        name: "Default container",
                    });
                } else if (!identitiesStore[store.id]) {
                    identities.push({
                        cookieStoreId: store.id,
                        color: 'black',
                        colorCode: '#000000',
                        name: "Store " + store.id + " (not a container)",
                    });
                }
            }

            return Promise.all(
                identities.map(identity =>
                    browser.cookies.getAll({
                        storeId: identity.cookieStoreId,
                    }).then(cookieList => {
                        let byDomain = {};
                        for (let cookie of cookieList) {
                            if (!byDomain[cookie.domain]) {
                                byDomain[cookie.domain] = [];
                            }

                            byDomain[cookie.domain].push(cookie);
                        }

                        let cookies = [];
                        for (let domain in byDomain) {
                            cookies.push({
                                domain: domain,
                                cookies: byDomain[domain],
                            });
                        }

                        return {
                            identity: identity,
                            cookies: cookies,
                        };
                    })
                )
            );
        })
    ).then(results => {
        let template = document.getElementById('cookie-list-template').innerHTML;
        let rendered = Mustache.render(template, {results: results});
        document.getElementById('cookie-list-target').innerHTML = rendered;

        for (let checkbox of document.getElementsByTagName("input")) {
            if (!checkbox.getAttribute('domain') && checkbox.getAttribute('store')) {
                checkbox.addEventListener('click', () => {
                    checkSub(checkbox);
                });
            }
        }
    });
}

function eraseCookies() {
    Promise.all(
        toList(document.getElementsByTagName('input')).map(checkbox => {
            if (!checkbox.checked) {
                return Promise.resolve();
            }

            let domain = checkbox.getAttribute('domain')
            let store = checkbox.getAttribute('store');
            if (!domain || !store) {
                return Promise.resolve();
            }

            return browser.cookies.getAll({
                domain: domain,
                storeId: store,
            }).then(cookies =>
                Promise.all(
                    cookies.map(cookie => {
                        let url = cookie.secure ? 'https://' : 'http://';
                        url += cookie.domain;
                        url += cookie.path || '/';

                        return browser.cookies.remove({
                            name: cookie.name,
                            storeId: cookie.storeId,
                            firstPartyDomain: cookie.firstPartyDomain,
                            url: url,
                        });
                    })
                )
            );
        })
    ).then(() => {
        showCookies();
    });
}

function checkSub(elt) {
    for (let checkbox of document.getElementsByTagName("input")) {
        if (checkbox.getAttribute('domain') && checkbox.getAttribute('store') == elt.getAttribute('store')) {
            checkbox.checked = elt.checked;
        }
    }
}
