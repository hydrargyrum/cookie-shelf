function parseRules(text) {
    let ret = {};
    for (let line of text.split('\n')) {
        line = line.trim();
        if (line.startsWith('#')) {
            continue;
        }

        let parts = line.split(':');
        let storeId = parts[0].trim();

        if (!storeId) {
            continue;
        }

        if (!ret[storeId]) {
            ret[storeId] = [];
        }

        for (let ruleStr of parts[1].split(',')) {
            ruleStr = ruleStr.trim();

            if (ruleStr[0] == '-' || ruleStr[0] == '+') {
                ret[storeId].push(ruleStr);
            }
        }
    }
    return ret;
}

function domainMatch(domain, pattern) {
    if (pattern == '*') {
        return true;
    }

    if (/^[a-z0-9.*-]+$/.test(pattern)) {
        pattern = pattern.replace('.', '\\.');
        pattern = pattern.replace('*', '.*');
        pattern = '^.*\\b' + pattern + '$';
    }

    let re = new RegExp(pattern);
    return re.test(domain);
}

function storeRulesMatch(domain, storeRules) {
    if (!storeRules) {
        return false;
    }

    for (let rule of storeRules) {
        if (domainMatch(domain, rule.substring(1))) {
            return rule[0] == '+' ? false : true;
        }
    }
}

browser.cookies.onChanged.addListener((event) => {
    if (event.removed) {
        return;
    }

    let storeId = event.cookie.storeId;

    let domain = event.cookie.domain;
    if (domain[0] == '.') {
        domain = domain.substring(1);
    }

    browser.storage.local.get('rules').then(store => {
        let rules = parseRules(store['rules'] || '');

        if (storeRulesMatch(domain, rules[storeId]) || storeRulesMatch(domain, rules['*'])) {
            console.debug('deleting cookie', event.cookie.name, 'for', event.cookie.domain, 'in', storeId);
            browser.cookies.remove({
                firstPartyDomain: event.cookie.firstPartyDomain,
                name: event.cookie.name,
                storeId: storeId,
                url: 'https://' + domain,
            });
        }
    });
});

browser.runtime.onConnect.addListener((event) => {

});
