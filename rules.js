window.addEventListener('load', () => {
    browser.storage.local.get('rules').then(store => {
        document.getElementById('rules-editor').value = store['rules'] || "";
    });

    document.getElementById('rules-button').addEventListener('click', setRules);
});

function setRules() {
    browser.storage.local.set({
        rules: document.getElementById('rules-editor').value,
    });
    //console.log(parseRules(document.getElementById('rules-editor').value));
}
